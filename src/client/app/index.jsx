import React from 'react';
import { render } from 'react-dom';
import update from 'immutability-helper';
import AwesomeComponent from './AwesomeComponent.jsx';


class UserList extends React.Component {
    constructor() {
        super();
        this.state = { userList: [] };
        this.addUserToList = this.addUserToList.bind(this);
    }

    addUserToList(username) {
        var newState = update(this.state, {
            userList: {
                $push: [username]
            }
        });
        this.setState(newState);
    }

    removeUserFromList(username) {
        var newState = update(this.state, {
            userList: {
                $pop: [username]
            }
        });
        this.setState(newState);
    }

    render() {
        return (
            <ul className="list-group" id="onlineUsersList">
                {this.state.userList.map(((username, index) =>
                    <li className="list-group-item" key={index}>{username}</li>))}
            </ul>
        );
    }
}

class ControlPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userList: props.userList
        };

    }

    setUserList(userList) {
        this.setState(prevState => ({
            userList: userList
        }));
    }

    render() {
        return (
            <aside className="col-xs-1 col-md-2 col-sm-3 left-column">
                <div>
                    <div id="manageRoomsDiv" className="centered-text">
                        <button className="btn">Create Room</button>
                        <button className="btn">Manage Rooms</button>
                    </div>
                    <div className="onlineUsersDiv">
                        {this.state.userList}
                    </div>
                </div>
            </aside>
        );
    }
}

class ChatPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            chatLog: props.chatLog,
            inputTextContent: ''
        }

        this.__on_input_key_down = this.__on_input_key_down.bind(this);
        this.__on_change_text = this.__on_change_text.bind(this);
    }

    setChatLog(chatLog) {
        this.setState(prevState => ({
            chatLog: chatLog
        }));
    }

    __on_input_key_down(event) {
        event.persist();
        let keycode = event.keycode || event.which;
        
        if (keycode == 13 && !event.shiftKey) { // The 'enter' key
            // Send the message
            console.log("sending message...");
            event.preventDefault();
            event.stopPropagation();
            // Call the event handler passed from props.
            this.props.onSendMessage(this.state.inputTextContent);
            this.setState(prevState => ({
                inputTextContent: ""
            }));
        }
    }

    __on_change_text(event) {
        event.persist();
        let newCharacter = event.target.value;
        this.setState(prevState => ({
            inputTextContent: newCharacter
        }));
        event.stopPropagation();
        event.preventDefault();
    }

    render() {
        return (
            <div id="ChatDiv" className="col-xs-11 col-md-10 col-sm-9">
                <div id="chat-div-box">
                    <div className="message-list-wrapper">
                        {this.state.chatLog}
                    </div>
                    <div id="input-div">
                       <textarea onKeyDown={this.__on_input_key_down} onChange={this.__on_change_text} className="auto-expand" id="chat-input-box" value={this.state.inputTextContent}>
                       </textarea>
                    </div>
                </div>
            </div>
        );
    }
}

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: props.user,
            message: props.message
        };
    }

    render() {
        return (
            <li>
                <font>{this.state.user + ': '}</font>
                <font>{this.stat.message}</font>
            </li>
        );
    }
}

class ChatLog extends React.Component {
    constructor(props) {
        super(props);
        this.state = { messages: [] };
    }

    addMessage(user, message) {
        var newState = update(this.state, {
            messages: {
                $push: <Message user={user} message={message} key={this.state.messages.length} />
            }
        });
    }

    render() {
        return (
            <ul>
                {this.state.messages.map((message) =>
                    message)}
            </ul>
        );
    }
}

class ChatThread {
    constructor(chatRoomName) {
        this.__chatRoomName = chatRoomName;
        //this.__chatRoomUrl = "ws://" + window.location.host + "/chat/" + this.__chatRoomName;
        this.__chatRoomUrl = "ws://" + "127.0.0.1:8080" + "/chat/" + this.__chatRoomName;
        this.__websocket = new WebSocket(this.__chatRoomUrl);
        this.__websocket.handler = this;
        this.__on_websocket_open = this.__on_websocket_open.bind(this);
        this.__on_websocket_receive = this.__on_websocket_receive.bind(this);
        this.__websocket.onopen = this.__on_websocket_open;
        this.__websocket.onmessage = this.__on_websocket_receive;
        this.__userList = <UserList />;
        this.__chatLog = <ChatLog />;
        this.__userConnected = this.__userConnected.bind(this);
        this.__userDisconnected = this.__userDisconnected.bind(this);
        this.__messageReceived = this.__messageReceived.bind(this);
    }

    __on_websocket_open() {
        console.log("Connected!");
    }

    __on_websocket_receive(event) {
        jsonData = JSON.parse(event.data);
        let messageType = jsonData["type"];
        let messageSender = jsonData["sender"];
        let messageContent = jsonData["content"];

        if (messageSender === 1) {
            this.__userConnected();
        } else if (messageType === 2) {
            this.__userDisconnected();
        } else if (messageType === 3) {
            this.__messageReceived();
        }
        console.log("Message received: " + event.data);
    }

    __userConnected(username) {
        console.log(username + " connected");
    }

    __userDisconnected(username) {
        console.log(username + " disconnected");
    }

    __messageReceived(username, message) {
        console.log(username + ": " + message);
    }
    getChatLog() {
        return this.__chatLog;
    }

    getUserList() {
        return this.__userList;
    }
}

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            chatThreads: [new ChatThread("WebChat1")]
        };

    }
    render() {
        return (
            <div>
                <ControlPanel userList={this.state.chatThreads[0].getUserList()} />
                <ChatPanel chatLog={this.state.chatThreads[0].getChatLog()} />
            </div>
        );
    }
}

render(<App />, document.getElementById('app'));
